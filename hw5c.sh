#!/usr/bin/env bash

# create language models from unknown language files
./train.py /opt/dropbox/12\-13/570/hw5/train $1 lms_test testing

# one those files are made, calculate divergence
./divergence.py lms_test lms output5c.txt
