#!/usr/bin/env python2.7

# Stefan Behr
# LING 570
# Homework 5
# 11/24/2012

import cPickle, codecs, os
from glob import glob
from numpy import linalg
from math import exp, log

def get_vocab(train, test, enc):
    txt = "*.txt"
    fnames = glob(os.path.join(train, txt)) + glob(os.path.join(test, txt))

    return list(set(" ".join(" ".join(codecs.open(fname, encoding=enc).read().strip().split())
                              for fname in fnames)))

def c_star(c, k, N_table):
    """
    Given a count c, count threshold k, and table of frequencies
    of counts, return c*, the smoothed count for c.
    """
    c_star =  ((float(c + 1) * N_table[c + 1] / N_table[c] -
             float(c) * (k + 1) * N_table[k+1] / N_table[1]) /
            (1 - float(k + 1) * N_table[k + 1] / N_table[1]))    
    return c_star

def ngram_cstar(ngram_counts, k, V):
    """
    Given a dict of ngram counts, returns smoothed
    counts for ngrams with frequency 0 <= c <= k
    using Good-Turing smoothing.
    """
    max_count = k + 1
    n = len(ngram_counts.keys()[0])
    
    N_table = {c: 0 for c in range(max_count + 1)}
    for c in ngram_counts.values():
        if c < max_count + 1:
            N_table[c] += 1

    N_table[0] = V ** n - len(ngram_counts)
    N_table = smoothcountfreqs(N_table)

    c_star_table = {c: c_star(c, k, N_table) for c in N_table if c > 0 and c < max_count}
    c_star_table[0] = N_table[1] / float(N_table[0])

    return c_star_table

def smoothcountfreqs(N_table):
    """
    Returns smoothed count frequencies using logistic
    linear regression.
    """
    b, a = loglinregression(N_table)
    for c in N_table:
        if c > 0:
            N_table[c] = exp(a + b * log(c))
    return N_table

def loglinregression(N_table):
    """
    Returns coefficients of linear regression of
    frequencies of frequencies on frequencies.
    """
    counts = [c for c in N_table.keys() if c > 0]
    count_freqs = [N_table[count] for count in counts]
    b, a = linalg.lstsq(zip(map(log, counts), (1,) * len(counts)), map(log, count_freqs))[0]
    return (b, a)

class LM:
    def __init__(self, fpath, voc):
        """
        Initialize language model object, reading in 
        the file at the given path, obtaining character 
        unigram, bigram, and trigram counts.
        """
        with codecs.open(fpath, encoding="latin1") as language:
            chars = language.read().strip()
        chars = " ".join(chars.split())

        self.uni = {}
        self.bi = {}
        self.tri = {}

        self.voc = voc

        i = 0
        n = len(chars)
        num_bigrams = n - 1
        num_trigrams = n - 2
        for char in chars:
            self.uni[char] = self.uni.get(char, 0) + 1
            if i < num_bigrams:
                bigram = chars[i:i+2]
                self.bi[bigram] = self.bi.get(bigram, 0) + 1
            if i < num_trigrams:
                trigram = chars[i:i+3]
                self.tri[trigram] = self.tri.get(trigram, 0) + 1
            i += 1

    def smooth(self, k=5):
        """
        Smooth unigram, bigram, and trigram counts, using
        Good-Turing smoothing for ngram counts where n > 1 
        and Add-1 smoothing for unigrams.
        """

        V = len(self.voc)

        # unigram smoothing, add-1
        for char in self.voc:
            self.uni[char] = self.uni.get(char, 0) + 1

        # good-turing smoothing for bigrams
        self.bi_recounts = ngram_cstar(self.bi, k, V)

        # and trigrams
        self.tri_recounts = ngram_cstar(self.tri, k, V)

    def make_probabilities(self, k):
        """
        Using an LM object's smoothed unigram, bigram, and 
        trigram counts, create 1-, 2-, and 3-dimensional probability
        vectors. Returns dict of these probability vectors.
        """
        self.uni_p, self.bi_p, self.tri_p = {}, {}, {} # probability vertex, matrix, lattice

        N = sum(self.uni.values())

        # unigram probability vector loop
        for a in self.voc:
            c_a = float(self.uni[a])
            p_a = c_a / N
            self.uni_p[a] = p_a
            # bigram probability matrix loop
            for b in self.voc:
                ab = a+b
                c_ab = self.bi.get(ab, 0)
                if c_ab < k + 1: # below smoothing threshold?
                    c_ab = self.bi_recounts[c_ab]
                c_ab = float(c_ab)
                p_ab = c_ab / c_a
                self.bi_p[ab] = p_ab
                # trigram probability lattice loop
                for c in self.voc:
                    abc = a+b+c
                    c_abc = self.tri.get(abc, 0)
                    if c_abc < k + 1:
                        c_abc = self.tri_recounts[c_abc]
                    c_abc = float(c_abc)
                    p_abc = c_abc / c_ab
                    self.tri_p[abc] = p_abc

    def save(self, fpath):
        with open(fpath, "w") as out:
            cPickle.dump({'u': self.uni_p,
                          'b': self.bi_p,
                          't': self.tri_p}, out)
