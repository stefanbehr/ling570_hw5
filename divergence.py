#!/usr/bin/env python2.7

# Stefan Behr
# LING 570
# Homework 5
# 11/24/2012

def format_matrix(matrix):
    """
    Given a matrix represented as a dict of dicts,
    return a nicely formatted string representation 
    of the matrix for pretty-printing/writing to file.
    """
    row_labels = sorted(matrix.keys())
    col_labels = sorted(matrix.values()[0].keys())
    max_width = max(map(len, col_labels))
    fix_width = lambda s: "{1:{0}s}".format(max_width, s) # function for fixing cell width
    rows = ["\t".join(map(fix_width, [''] + col_labels))]
    for row_label in row_labels:
        row = [row_label]
        for col_label in col_labels:
            cell = "{0:.2f}".format(matrix[row_label][col_label])
            row.append(cell)
        rows.append("\t".join(map(fix_width, row)))
    return "\n".join(rows)
    

if __name__ == "__main__":
    import cPickle, os, sys
    from glob import glob
    from math import log
    
    if len(sys.argv) < 2:
        exit("Please provide a path to a language model directory (for rows).")
    elif len(sys.argv) < 3:
        exit("Please provide a path to a language model directory (for columns).")
    elif len(sys.argv) < 4:
        exit("Please provide a filepath for saving table output.")
    else:
        lm_dir_r = sys.argv[1]
        lm_dir_c = sys.argv[2]
        savepath = sys.argv[3]

        if not (os.path.isdir(lm_dir_r) and os.path.isdir(lm_dir_c)):
            exit("Invalid LM directory provided.")
    
        lm_paths_r = glob(os.path.join(lm_dir_r, '*.lm'))
        lm_paths_c = glob(os.path.join(lm_dir_c, '*.lm'))
        # lm_paths = map(lambda p: os.path.join(lm_dir, p), ['polish.lm', 'english.lm', 'slovak.lm'])
        lm_labels_r = {lm_path: os.path.splitext(os.path.split(lm_path)[1])[0].replace('_', ' ').title() for lm_path in lm_paths_r}
        lm_labels_c = {lm_path: os.path.splitext(os.path.split(lm_path)[1])[0].replace('_', ' ').title() for lm_path in lm_paths_c}

        kl_div_matrix = {} # will represent KL-divergence matrix
    
        # iterate through matrix of languages, calculate KL-divergence for each
        for row_lm_path in lm_paths_r:
            row_label = lm_labels_r[row_lm_path] # map filepath to pretty label
            with open(row_lm_path) as f:
                row_lm = cPickle.load(f)
            row_uni = row_lm['u']
            row_bi = row_lm['b']
            row_tri = row_lm['t']
    
            kl_div_row = {}
            for col_lm_path in lm_paths_c:
                col_label = lm_labels_c[col_lm_path]
                if col_label == row_label:
                    kl_div_row[col_label] = 0
                else:
                    with open(col_lm_path) as f:
                        col_lm = cPickle.load(f)
                    col_uni = col_lm['u']
                    col_bi = col_lm['b']
                    col_tri = col_lm['t']
    
                    kl_div = 0
    
                    # calculate trigram probs over all trigrams
                    # xyz, and calculate
                    for x in row_uni:
                        q_x = row_uni[x]
                        p_x = col_uni[x]
                        for y in row_uni:
                            xy = x + y # create bigram
                            q_yGx = row_bi[xy] # row model's P(y|x)
                            p_yGx = col_bi[xy] # col model's P(y|x)
                            for z in row_uni:
                                xyz = xy + z # create trigram
                                q_zGxy = row_tri[xyz] # row model's P(z|xy)
                                p_zGxy = col_tri[xyz] # col model's P(z|xy)
    
                                q_xyz = q_x * q_yGx * q_zGxy
                                p_xyz = p_x * p_yGx * p_zGxy
    
                                kl_div += p_xyz * log(p_xyz / q_xyz)
    
                    kl_div_row[col_label] = kl_div
    
            kl_div_matrix[row_label] = kl_div_row
    
        # save KL-divergence matrix to file
        with open(savepath, "w") as savefile:
            savefile.write(format_matrix(kl_div_matrix))
