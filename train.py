#!/usr/bin/env python2.7

# Stefan Behr
# LING 570
# Homework 5
# 11/24/2012

import os, sys
import utils
from glob import glob

if __name__ == "__main__":
    if len(sys.argv) < 2:
        exit("Please provide a training source directory.")
    elif len(sys.argv) < 3:
        exit("Please provide a testing source directory.")
    elif len(sys.argv) < 4:
        exit("Please provide a destination directory for language models.")
    elif len(sys.argv) < 5:
        exit("Please provide a mode -- either 'training' or 'testing'.")
    else:
        train_dir = sys.argv[1]
        if not os.path.isdir(train_dir):
            exit("Training directory given not recognized.")

        test_dir = sys.argv[2]
        if not os.path.isdir(test_dir):
            exit("Testing directory given not recognized.")

        lm_dir = sys.argv[3]
        if not os.path.isdir(lm_dir):
            os.mkdir(lm_dir, 0755)

        mode = sys.argv[4]
        modes = ("training", "testing")
        if mode not in modes:
            exit("Invalid mode. Must be one of: {0}".format(", ".join("'{0}'".format(mode) for mode in modes)))

        try:
            subset = int(sys.argv[5])
        except (IndexError, ValueError):
            subset = 0

        ENC = "latin1"
        K = 5
        
        voc = utils.get_vocab(train_dir, test_dir, ENC)

        data_dir = train_dir
        if mode == "testing":
            data_dir = test_dir
        fpath_pattern = os.path.join(data_dir, "*.txt")

        # loop through training files, create an LM for each
        i = 0
        for fpath in sorted(glob(fpath_pattern)):
            if not subset or i < subset:
                lm = utils.LM(fpath, voc) # init LM, get counts
                lm.smooth(K) # adjust counts (add-1 smoothing for unigrams, GT for bi-/tri-grams)
                lm.make_probabilities(K) # calculate probabilities from final counts
                head, tail = os.path.split(fpath)
                shortname, ext = os.path.splitext(tail)
                fpath = os.path.join(lm_dir, shortname + ".lm")
                lm.save(fpath)
            i += 1
