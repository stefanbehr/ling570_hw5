Stefan Behr
LING 570
Homework 5
11/24/2012

Dependencies and Side Effects
=============================
After unarchiving the contents of hw5.tar, one may run hw5a.cmd immediately without further setup.
hw5a.cmd will produce (or modify) a directory called 'lms' in the current working directory. The code
run by hw5a.cmd will generate the required character trigram language models and write them to .lm files 
in the 'lms' directory. The .lm files are not particularly amenable to direct inspection, as they contain 
the results of using the cPickle module to serialize Python dicts.

hw5b.cmd depends on the existence of the aforementioned 'lms' directory. Therefore, hw5a.cmd must be 
successfully run before hw5b.cmd. The running time of the code used by hw5b.cmd, assuming the use of the 
training set we were given for generating language models, is a little over one hour. The KL-divergence table
for the part of the homework corresponding to this Condor script is saved to a file named output5b.txt, as
required.

hw5c.cmd generates language models for the test files in a given directory -- the models are stored in a
newly created directory called 'lms_test'. hw5c.cmd also compares these new language 
models to ones that were generated by running hw5a.cmd, which reside in the aforementioned 'lms' directory.
Therefore, the ability to run hw5c.cmd also depends on the prior successful execution of hw5a.cmd. The
KL-divergence table for this part of the homework is written to output5c.txt. The running time of hw5c.cmd
was roughly 40 minutes on patas.

Python Modules
--------------
All Python module dependencies in my code are satisfied by the Python install on patas. I cannot guarantee
that all required modules (e.g., cPickle, numpy, etc.) are present on any other system where my code might
be executed.
